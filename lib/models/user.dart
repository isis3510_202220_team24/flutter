import 'package:flutter/material.dart';

class User {
  late final String id;
  final String name;
  final String username;
  final String password;
  late final String? imagen;

  User(
      {Key? key,
      required this.id,
      required this.name,
      required this.username,
      required this.password,
      this.imagen});

  Map<String, dynamic> toJson() =>
      {'id': id, 'name': name, 'username': username, 'password': password};

  static User fromJson(Map<String, dynamic> json) => User(
      id: json['id'], name: json['name'], username: json['username'], password: json['password']);
}
