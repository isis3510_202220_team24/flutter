import 'package:petsbnb/models/imagen.dart';
import 'package:petsbnb/models/user.dart';

class Place {
  String? id;
  String? titulo;
  String? descripcion;
  String? ciudad;
  String? barrio;
  Ubicacion? ubicacion;
  int? precio;

  List<Imagen>? imagenes;

  Place({
    this.id,
    this.titulo,
    this.descripcion,
    this.ciudad,
    this.barrio,
    this.ubicacion,
    this.precio,
    this.imagenes,
  });

  Place.fromJson(Map<String, dynamic> json) {
    if (json['imagenes'] != null) {
      imagenes = <Imagen>[];
      json['imagenes'].forEach((v) {
        imagenes!.add(Imagen.fromJson(v));
      });
    }
    id = json['id'];
    titulo = json['titulo'];
    descripcion = json['descripcion'];
    ciudad = json['ciudad'];
    barrio = json['barrio'];
    ubicacion = json['ubicacion'] != null
        ? Ubicacion.fromJson(json['ubicacion'])
        : null;
    precio = json['precio'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['titulo'] = titulo;
    data['descripcion'] = descripcion;
    data['ciudad'] = ciudad;
    data['barrio'] = barrio;
    if (ubicacion != null) {
      data['ubicacion'] = ubicacion!.toJson();
    }
    if (imagenes != null) {
      data['imagenes'] = imagenes!.map((v) => v.toJson()).toList();
    }
    data['precio'] = precio;
    return data;
  }
}

class Ubicacion {
  double? latitud;
  double? longitud;

  Ubicacion({this.latitud, this.longitud});

  Ubicacion.fromJson(Map<String, dynamic> json) {
    latitud = json['latitud'];
    longitud = json['longitud'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['latitud'] = latitud;
    data['longitud'] = longitud;
    return data;
  }
}
