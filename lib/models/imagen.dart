import 'dart:io';

class Imagen {
  String? id;
  String? url;
  int? order;
  File? archivo;

  Imagen({this.id, this.url, this.order, this.archivo});

  Imagen.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    url = json['url'];
    order = json['order'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['url'] = url;
    data['order'] = order;
    return data;
  }
}
