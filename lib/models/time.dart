class Time {
  String? id;
  String? method;
  int? time;

  Time({
    this.id,
    this.method,
    this.time,
  });

  Time.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    method = json['method'];
    time = json['time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['method'] = method;
    data['time'] = time;
    return data;
  }
}
