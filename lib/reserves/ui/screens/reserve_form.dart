import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

class ReserveForm extends StatefulWidget {
  const ReserveForm({super.key});

  @override
  State<ReserveForm> createState() => _ReserveFormState();
}

class _ReserveFormState extends State<ReserveForm> {
  String imgUrl =
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQAmCl7V0QO4l8h1psqzO1t4766F2ArIYo0Ow&usqp=CAU';
  final formKey = GlobalKey<FormState>();
  TextEditingController? commentController;
  String? comment;
  String finishDate = "";
  String initialDate = "";
  bool multipleDays = false;
  double costday = 10.00;
  double costhour = 2.00;
  double totalcost = 0.00;
  bool hasInternet = true;
  var _currentSelectedDay = DateTime.now();
  var _currentSelectedDate =
      DateTimeRange(start: DateTime.now(), end: DateTime.now());
  var _currentSelectedTime = TimeOfDay.now();
  var _currentSelectedEndTime = TimeOfDay.now();

  void callDayDatePicker() async {
    var selectedDate = await getDayDatePickerWidget();
    setState(() {
      _currentSelectedDay = selectedDate!;
    });
  }

  void callDatePicker() async {
    getDatePickerWidget().then((value) => {
      setState(() {
        _currentSelectedDate = value!;
        if (multipleDays) {
          totalcost = costday * _currentSelectedDate.duration.inDays;
        }
      })
    });
  }

  void callTimePicker() async {
    var selectedTime = await getTimePickerWidget();
    setState(() {
      _currentSelectedTime = selectedTime!;
      if (!multipleDays) {
        initialDate =
            "${_currentSelectedDay.day}/${_currentSelectedDay.month}/${_currentSelectedDay.year} ${_currentSelectedTime.hour.toString()}:${_currentSelectedTime.minute.toString()}";
      } else {
        initialDate =
            "${_currentSelectedDate.start.day}/${_currentSelectedDate.start.month}/${_currentSelectedDate.start.year} ${_currentSelectedTime.hour.toString()}:${_currentSelectedTime.minute.toString()}";
      }
      print(initialDate);
    });
  }

  void callEndTimePicker() async {
    var selectedTime = await getEndTimePickerWidget();
    setState(() {
      _currentSelectedEndTime = selectedTime!;
      if (!multipleDays) {
        totalcost = costhour *
            (_currentSelectedEndTime.hour - _currentSelectedTime.hour);

        finishDate =
            "${_currentSelectedDay.day}/${_currentSelectedDay.month}/${_currentSelectedDay.year} ${_currentSelectedEndTime.hour.toString()}:${_currentSelectedEndTime.minute.toString()}";
      } else {
        finishDate =
            "${_currentSelectedDate.end.day}/${_currentSelectedDate.end.month}/${_currentSelectedDate.end.year} ${_currentSelectedEndTime.hour.toString()}:${_currentSelectedEndTime.minute.toString()}";
      }
      print(finishDate);
    });
  }

  Future<DateTime?> getDayDatePickerWidget() {
    return showDatePicker(
        context: context,
        initialDate: _currentSelectedDay,
        firstDate: DateTime.now(),
        lastDate: DateTime(2024));
  }

  Future<DateTimeRange?> getDatePickerWidget() {
    return showDateRangePicker(
        context: context, firstDate: DateTime.now(), lastDate: DateTime(2024));
  }

  Future<TimeOfDay?> getTimePickerWidget() {
    return showTimePicker(context: context, initialTime: _currentSelectedTime);
  }

  Future<TimeOfDay?> getEndTimePickerWidget() {
    return showTimePicker(
        context: context, initialTime: _currentSelectedEndTime);
  }

  @override
  Widget build(BuildContext context) {
    final customCacheManager = CacheManager(
      Config(
        'customCacheKeyR',
        stalePeriod: const Duration(days: 15),
        maxNrOfCacheObjects: 5,
      ),
    );
    Widget imageheader() {
      return Container(
        alignment: Alignment.center,
        child: CachedNetworkImage(
            key: UniqueKey(),
            fit: BoxFit.fill,
            width: 300,
            height: 200,
            cacheManager: customCacheManager,
            imageUrl: imgUrl),
      );
    }

    Widget boolPick() {
      return Container(
        margin: EdgeInsets.only(top: 20, bottom: 50),
        child: SwitchListTile(
          value: multipleDays,
          title: Text('Multiple days?'),
          onChanged: (val) {
            setState(() {
              multipleDays = val;
            });
          },
        ),
      );
    }

    Widget costs() {
      if (multipleDays) {
        return Container(
          margin: EdgeInsets.only(
            top: 20,
          ),
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Text(initialDate),
              Text(finishDate),
              Text('total cost: $totalcost')
            ],
          ),
        );
      } else {
        return Container(
          margin: EdgeInsets.only(
            top: 20,
          ),
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Text(initialDate),
              Text(finishDate),
              Text('total cost: $totalcost')
            ],
          ),
        );
      }
    }

    Widget form() {
      if (multipleDays) {
        return Form(
            key: formKey,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        Text("range of days"),
                        ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.grey),
                            onPressed: callDatePicker,
                            child: Icon(Icons.calendar_month)),
                      ],
                    ),
                    Column(
                      children: [
                        Text("start hour"),
                        ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.grey),
                            onPressed: callTimePicker,
                            child: Icon(Icons.alarm)),
                      ],
                    ),
                    Column(
                      children: [
                        Text("end hour"),
                        ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.grey),
                            onPressed: callEndTimePicker,
                            child: Icon(Icons.alarm)),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 50,
                ),
                Text(
                    "days: " + _currentSelectedDate.duration.inDays.toString()),
                TextFormField(
                  maxLines: 5,
                  controller: commentController,
                  decoration: InputDecoration(labelText: 'Comments'),
                  onSaved: (value) {
                    comment = value;
                  },
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Llene este campo';
                    }
                  },
                ),
              ],
            ));
      } else {
        return Form(
            key: formKey,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        Text("day"),
                        ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.grey),
                            onPressed: callDayDatePicker,
                            child: Icon(Icons.calendar_month)),
                      ],
                    ),
                    Column(
                      children: [
                        Text("start hour"),
                        ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.grey),
                            onPressed: callTimePicker,
                            child: Icon(Icons.alarm)),
                      ],
                    ),
                    Column(
                      children: [
                        Text("end hour"),
                        ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.grey),
                            onPressed: callEndTimePicker,
                            child: Icon(Icons.alarm)),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 50,
                ),
                Text("hours: " +
                    (_currentSelectedEndTime.hour - _currentSelectedTime.hour)
                        .toString()),
                TextFormField(
                  maxLines: 5,
                  controller: commentController,
                  decoration: InputDecoration(labelText: 'Comments'),
                  onSaved: (value) {
                    comment = value;
                  },
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Llene este campo';
                    }
                  },
                ),
              ],
            ));
      }
    }

    Widget botton() {
      return Container(
        margin: EdgeInsets.only(top: 50, bottom: 20),
        child: ElevatedButton(
            onPressed: () async {
              hasInternet = await InternetConnectionChecker().hasConnection;
              final text = hasInternet ? 'Internet' : 'No Internet';
              final color = hasInternet ? Colors.green : Colors.red;
              if (hasInternet) {
                if (totalcost > 0 &&
                    finishDate != "" &&
                    initialDate != "" &&
                    (_currentSelectedEndTime.hour - _currentSelectedTime.hour) >
                        0) {
                  onPressed(context);
                } else {
                  print(totalcost);
                  print((_currentSelectedEndTime.hour -
                      _currentSelectedTime.hour));
                  print(_currentSelectedDate.duration.inDays);
                  print(finishDate);
                  final snack = SnackBar(content: Text('choose a valid date'));
                  ScaffoldMessenger.of(context)..showSnackBar(snack);
                }
                print('conec');
              } else {
                final snack = SnackBar(content: Text('No internet connection'));
                ScaffoldMessenger.of(context)..showSnackBar(snack);
                print(' no conec');
              }
            },
            child: Text('Complete reserve')),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Complete your reserve"),
      ),
      body: Container(
          margin: EdgeInsets.all(20),
          alignment: Alignment.center,
          child: ListView(
            children: [imageheader(), boolPick(), form(), costs(), botton()],
          )),
    );
  }

  Future createReserve(
      {required String? comment,
      required double? cost,
      required String? finishDate,
      required String? initialDate,
      required bool multipleDays}) async {
    final docReserve = FirebaseFirestore.instance.collection('reserves').doc();
    final json = {
      'comment': comment,
      'cost': cost,
      'finishDate': finishDate,
      'initialDate': initialDate,
      'multipleDays': multipleDays,
    };

    await docReserve.set(json);
  }

  onPressed(BuildContext context) {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();

      createReserve(
          comment: comment,
          cost: totalcost,
          finishDate: finishDate,
          initialDate: initialDate,
          multipleDays: multipleDays).then((value) => {
            print("Reserve done")
      });
      //storage.uploadFile(path, nameFile);
      Navigator.of(context).pop();
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    commentController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    this.commentController!.dispose();
  }
}
