import 'package:flutter/material.dart';

class BigButtonMain extends StatelessWidget {
  String title;
  String route;
  BigButtonMain({super.key, required this.title, required this.route});

  @override
  Widget build(BuildContext context) {
    void onPressed(BuildContext context) {
      Navigator.of(context).pushNamed(route);
    }

    return Container(
      height: 50,
      width: 50,
      margin: EdgeInsets.all(35.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
      ),
      child: ElevatedButton(
          child: Text(title),
          style: ElevatedButton.styleFrom(
            primary: Color.fromRGBO(255, 87, 87, 1),
            shape: CircleBorder(),
          ),
          onPressed: () {
            onPressed(context);
          }),
    );
  }
}
