import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomTextField extends StatelessWidget {
  CustomTextField({
    super.key,
    required this.textEditingController,
    required this.titleText,
    this.hintText = '',
    this.helperText = '',
    this.border,
    this.textStyle,
    this.width,
    this.keyboardType,
    this.maxLines = 1,
  });

  final String titleText;
  final String hintText;
  final String helperText;
  final TextEditingController textEditingController;
  final InputBorder? border;
  final TextStyle? textStyle;
  final double? width;
  final TextInputType? keyboardType;
  final int? maxLines;

  @override
  Widget build(BuildContext context) {
    const UnderlineInputBorder borderTextField = UnderlineInputBorder(
      borderSide: BorderSide(
        // color: Palette.darkBlue,
        width: 2.0,
      ),
    );
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          titleText,
          style: const TextStyle(
            // color: Palette.darkBlue,
            fontSize: 16,
            fontWeight: FontWeight.w600,
          ),
        ),
        SizedBox(
          width: width ?? Get.width - 40,
          child: TextFormField(
            maxLines: maxLines,
            style: textStyle,
            decoration: InputDecoration(
              helperText: helperText,
              hintText: hintText,
              errorStyle: const TextStyle(
                fontSize: 14,
                color: Colors.red,
                fontWeight: FontWeight.bold,
              ),
              enabledBorder: borderTextField,
              focusedBorder: borderTextField,
              errorBorder: borderTextField,
              focusedErrorBorder: borderTextField,
            ),
            controller: textEditingController,
            validator: (String? _) {
              if (textEditingController.text.isEmpty) {
                return 'Por favor, rellena este campo';
              } else {
                return null;
              }
            },
            keyboardType: TextInputType.emailAddress,
          ),
        ),
      ],
    );
  }
}
