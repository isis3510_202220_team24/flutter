import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

class StorageService {
  Future<String?> uploadPDF(String userID, String folder, File pdfFile) async {
    try {
      final fstorage = FirebaseStorage.instance;
      final File image = await File(pdfFile.absolute.path).create();
      final Reference storageRef = fstorage.ref().child('$folder');
      final UploadTask uploadTask = storageRef.putFile(image);
      return await (await uploadTask).ref.getDownloadURL();
    } catch (e) {
      return 'No funciona';
    }
  }
}

StorageService storageService = StorageService();
