import 'dart:developer' as dev;
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:petsbnb/models/imagen.dart';
import 'package:petsbnb/models/place.dart';
import 'package:petsbnb/services/firebase_service/storage_service.dart';

class PlaceService {
  final _firestore = FirebaseFirestore.instance;

  Future<bool> addPlace({
    required Map<String, dynamic> place,
    required List<Imagen> imagenes,
  }) async {
    try {
      dev.log('Voy a crear el place');
      final CollectionReference collRef = _firestore.collection('places');
      final result = await _firestore.collection('places').add(place);
      final DocumentReference docReferance = collRef.doc(result.id);
      final Place aux = Place.fromJson(place);
      aux.id = result.id;

      await _firestore
          .collection("places")
          .doc(docReferance.id)
          .set({...aux.toJson()});
      await addImage(imagenes: imagenes, placeId: aux.id!);
      dev.log('Terminó de crear todo');
      return true;
    } catch (e) {
      dev.log(e.toString());
      return false;
    }
  }

  Future<bool> addImage({
    required List<Imagen> imagenes,
    required String placeId,
  }) async {
    try {
      final Random random = Random();
      for (var imagen in imagenes) {
        final int imageId = random.nextInt(10000000);
        final urlResult = await storageService.uploadPDF(
            placeId, "places/$placeId/$imageId", imagen.archivo!);
        imagen.url = urlResult;

        final DocumentReference placeReference = await _firestore
            .collection('places')
            .doc(placeId)
            .collection('imagenes')
            .add(imagen.toJson());

        await placeReference.set({...imagen.toJson(), 'id': placeReference.id});
      }
      return true;
    } catch (e) {
      dev.log(e.toString());
      return false;
    }
  }

  Future<List<Place>> getPlaces() async {
    List<Place> places = [];
    final querySnapshot = await _firestore.collection('places').get();
    if (querySnapshot.docs.isEmpty) return [];
    for (final element in querySnapshot.docs) {
      final Place place =
          Place.fromJson(element.data() as Map<String, dynamic>);

      places.add(place);
    }

    for (final place in places) {
      place.imagenes = [];
      final imagenesSnapshot = await _firestore
          .collection('places')
          .doc(place.id)
          .collection('imagenes')
          .get();
      if (imagenesSnapshot.docs.isEmpty) place.imagenes = [];
      for (final element in imagenesSnapshot.docs) {
        place.imagenes!.add(
          Imagen.fromJson(element.data()),
        );
      }
    }

    return places;
  }
}

PlaceService placeService = PlaceService();
