import 'dart:developer' as dev;
import 'dart:math';



import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:petsbnb/models/imagen.dart';
import 'package:petsbnb/models/user.dart';
import 'package:petsbnb/services/firebase_service/storage_service.dart';

class UserService {
  final _firestore = FirebaseFirestore.instance;

  Future<bool> addUser({
    required Map<String, dynamic> user,
    required Imagen imagen,
  }) async {
    try {
  
      final CollectionReference collRef = _firestore.collection('users');
      final result = await _firestore.collection('users').add(user);
      final DocumentReference docReferance = collRef.doc(result.id);
      final User aux = User.fromJson(user);
      aux.id = result.id;

      await _firestore
      .collection("users")
      .doc(docReferance.id)
      .set({...aux.toJson()});

      await addImage(imagen: imagen, userId: aux.id!);

      return true;
    } catch (e) {
      dev.log(e.toString());
      return false;
    }
  }

  Future<bool> addImage({
    required Imagen imagen,
    required String userId,
  }) async {
    try {
      final Random random = Random();
      
      final int imageId = random.nextInt(10000000);
      final urlResult = await storageService.uploadPDF(
          userId, "users/$userId/$imageId", imagen.archivo!);
        imagen.url = urlResult;

      final DocumentReference userReference = await _firestore
      .collection('users')
      .doc(userId)
      .collection('imagenes')
      .add(imagen.toJson());

      await userReference.set({...imagen.toJson(), 'id': userReference.id});
     
      return true;
    } catch (e) {
      dev.log(e.toString());
      return false;
    }
  }

  Future <User> getUser(
     String userId,
  ) async {
    User user =  User(id: '', name: '', username: '',password: '');
    final querySnapshot = await _firestore.collection('users').where("id", isEqualTo: userId).get();
    if (querySnapshot.docs.isEmpty) return user;
    for (final element in querySnapshot.docs) {
      user = User.fromJson(element.data() as Map<String, dynamic>);
    }


    return user;
  }

  Future <User> updateUser(
    Map<String, dynamic> user,
  ) async {
    final User aux = User.fromJson(user);
    final CollectionReference collRef = _firestore.collection('users');
    final DocumentReference docReferance = collRef.doc(aux.id);

    final DocumentReference userReference = await _firestore
    .collection('users')
    .doc(aux.id);

    await userReference.set(aux);

    return aux;
  }
}

UserService userService = UserService();
