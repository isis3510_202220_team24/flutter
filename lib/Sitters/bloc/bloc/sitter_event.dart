part of 'sitter_bloc.dart';

abstract class SitterEvent extends Equatable {
  const SitterEvent();

  @override
  List<Object> get props => [];
}

class LoadSitters extends SitterEvent {}

class GetSitters extends SitterEvent {
  List<Object> get props => [];
}
