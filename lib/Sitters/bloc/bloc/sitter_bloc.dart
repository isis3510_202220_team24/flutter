import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_isolate/flutter_isolate.dart';
import 'package:path_provider/path_provider.dart';

import 'package:petsbnb/Sitters/model/sitter.dart';
import 'package:petsbnb/Sitters/repository/sitter_repo.dart';

part 'sitter_event.dart';
part 'sitter_state.dart';

class SitterBloc extends Bloc<SitterEvent, SitterState> {
  final SitterRepository sitterRepository;
  SitterBloc({required this.sitterRepository}) : super(SitterInitial()) {
    on<GetSitters>(((event, emit) async {
      try {
        List<Sitter> sitters = await sitterRepository.get();
        emit(SittersLoaded(sitters));
      } catch (e) {
        emit(SittersError("error"));
      }
    }));
  }
}
