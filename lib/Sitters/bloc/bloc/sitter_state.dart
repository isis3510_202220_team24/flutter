part of 'sitter_bloc.dart';

abstract class SitterState extends Equatable {
  const SitterState();

  @override
  List<Object> get props => [];
}

class SitterInitial extends SitterState {
  @override
  List<Object> get props => [];
}

class SittersError extends SitterState {
  final String error;
  SittersError(this.error);
  List<Object> get props => [error];
}

class SittersLoading extends SitterState {
  @override
  List<Object> get props => [];
}

class SittersLoaded extends SitterState {
  final List<Sitter> siters;
  SittersLoaded(this.siters);
  @override
  List<Object> get props => [siters];
}
