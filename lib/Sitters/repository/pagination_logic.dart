import 'package:flutter/cupertino.dart';
import 'package:petsbnb/Sitters/model/sitter.dart';
import 'package:petsbnb/Sitters/repository/sitter_repo.dart';

class PaginationLogic extends ChangeNotifier {
  List<Sitter>? sitters;
  bool loading = false;
  final SitterRepository repo = SitterRepository();

  void LoadData(int from, int limit) async {
    print('cogiendo info');
    loading = true;
    notifyListeners();

    final result = await repo.getLimit(from: from, limit: limit);
    if (sitters == null) {
      sitters = [];
    }
    sitters?.addAll(result);

    loading = false;
    notifyListeners();
  }
}
