import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:petsbnb/Sitters/model/sitter.dart';

class SitterRepository {
  final _fireCloud = FirebaseFirestore.instance.collection('sitters');
  Future<List<Sitter>> get() async {
    List<Sitter> sittersList = [];
    try {
      final sitters = await _fireCloud.get();
      sitters.docs.forEach((element) {
        return sittersList.add(Sitter.fromJson(element.data()));
      });
    } on FirebaseException catch (e) {
      if (kDebugMode) {
        print('Failed with error ${e.code}: ${e.message}');
        return sittersList;
      }
    } catch (e) {
      print("error en getSitter " + e.toString());
      throw Exception(e.toString());
    }
    return sittersList;
  }

  Future<List<Sitter>> getLimit({int? from, int? limit}) async {
    List<Sitter> sittersList = [];

    final sitters = await _fireCloud
        .orderBy('position')
        .startAfter([from])
        .limit(limit!)
        .get();
    sitters.docs.forEach((element) {
      sittersList.add(Sitter.fromJson(element.data()));
    });

    int n = sittersList.length;
    print('se encontraron sitterlist $n');
    return sittersList;
  }
}
