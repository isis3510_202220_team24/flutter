import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:petsbnb/Sitters/ui/widgets/stars_review.dart';

class SitterCard extends StatelessWidget {
  String name;
  String pic;
  String space;
  String age;
  num rating;
  SitterCard(
      {super.key,
      required this.age,
      required this.name,
      required this.space,
      required this.pic,
      required this.rating});

  @override
  Widget build(BuildContext context) {
    final customCacheManager = CacheManager(
      Config(
        'customCacheKeyS',
        stalePeriod: const Duration(days: 15),
        maxNrOfCacheObjects: 100,
      ),
    );
    Widget card = Card(
      elevation: 20,
      color: Color(0xfffededed),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
      child: Container(
        height: 150,
        width: 300,
        child: Center(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              height: 100,
              width: 100,
              margin: EdgeInsets.all(20),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(50.0)),
                shape: BoxShape.rectangle,
              ),
              child: CachedNetworkImage(
                  key: UniqueKey(),
                  fit: BoxFit.cover,
                  cacheManager: customCacheManager,
                  imageUrl: pic),
            ),
            Container(
              margin: EdgeInsets.only(top: 40, bottom: 20, left: 0, right: 20),
              child: Center(
                  child: Column(
                children: [
                  Row(
                    children: [
                      Text(name),
                      SizedBox(width: 5),
                      Text(age.toString()),
                    ],
                  ),
                  SizedBox(height: 10),
                  Text(space),
                  SizedBox(
                    height: 10,
                  ),
                  StarsReview(starnumber: rating),
                ],
              )),
            ),
            Container(
              margin: EdgeInsets.only(right: 10),
              width: 60,
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    shape: StadiumBorder(),
                  ),
                  onPressed: () {
                    onPressed(context);
                  },
                  child: Text(
                    "Hire",
                    style: TextStyle(fontSize: 14),
                  )),
            )
          ],
        )),
      ),
    );
    return card;
  }

  void onPressed(BuildContext context) {
    Navigator.of(context).pushNamed("/reserveform");
  }
}
