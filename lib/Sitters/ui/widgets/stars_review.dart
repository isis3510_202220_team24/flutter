import 'package:flutter/material.dart';

class StarsReview extends StatelessWidget {
  num starnumber;
  StarsReview({super.key, required this.starnumber});

  @override
  Widget build(BuildContext context) {
    final fullStar = Container(
      margin: const EdgeInsets.only(left: 3.0),
      child: const Icon(
        Icons.star,
        color: Colors.amber,
      ),
    );

    final halfStar = Container(
      margin: const EdgeInsets.only(left: 3.0),
      child: const Icon(
        Icons.star_half,
        color: Colors.amber,
      ),
    );

    final borderStar = Container(
      margin: const EdgeInsets.only(left: 3.0),
      child: const Icon(
        Icons.star_border,
        color: Colors.amber,
      ),
    );

    final zerostar = Row(
      children: [borderStar, borderStar, borderStar, borderStar, borderStar],
    );
    final onestar = Row(
      children: [fullStar, borderStar, borderStar, borderStar, borderStar],
    );
    final twostar = Row(
      children: [fullStar, fullStar, borderStar, borderStar, borderStar],
    );
    final threestar = Row(
      children: [fullStar, fullStar, fullStar, borderStar, borderStar],
    );
    final fourstar = Row(
      children: [fullStar, fullStar, fullStar, fullStar, borderStar],
    );
    final fivestar = Row(
      children: [fullStar, fullStar, fullStar, fullStar, fullStar],
    );
    if (starnumber == 1) {
      return onestar;
    } else if (starnumber == 2) {
      return twostar;
    } else if (starnumber == 3) {
      return threestar;
    } else if (starnumber == 4) {
      return fourstar;
    } else if (starnumber == 5) {
      return fivestar;
    } else {
      return zerostar;
    }
  }
}
