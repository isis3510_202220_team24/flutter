import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:petsbnb/Sitters/bloc/bloc/sitter_bloc.dart';
import 'package:petsbnb/Sitters/model/sitter.dart';
import 'package:petsbnb/Sitters/repository/pagination_logic.dart';
import 'package:petsbnb/Sitters/ui/widgets/sitter_card.dart';

class SittersList extends StatefulWidget {
  const SittersList({super.key});

  @override
  State<SittersList> createState() => _SittersListState();
}

class _SittersListState extends State<SittersList> {
  int index = 0;
  int limit = 5;
  final logic = PaginationLogic();
  final controller = ScrollController();
  void _onListener() {
    if ((controller.offset >= controller.position.maxScrollExtent) &&
        !logic.loading) {
      print('tocado');
      print('$index $limit');
      logic.LoadData(index, limit);
      setState(() {
        index = index + limit;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    controller.addListener(_onListener);
    logic.LoadData(index, limit);
    setState(() {
      index = index + limit;
      limit = 1;
    });
    super.initState();
  }

  @override
  void dispose() {
    controller.removeListener(_onListener);
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget buildSitter(Sitter sitter) => SitterCard(
        age: sitter.age,
        name: sitter.name,
        space: sitter.space,
        pic: sitter.pictureUrl,
        rating: sitter.rating);

    final sinbloc = AnimatedBuilder(
        animation: logic,
        builder: (_, __) {
          return logic.sitters != null
              ? ListView.builder(
                  controller: controller,
                  itemCount: logic.sitters?.length,
                  itemBuilder: (_, index) {
                    return buildSitter(logic.sitters![index]);
                  })
              : Center(child: CircularProgressIndicator());
        });

    final conbloc =
        BlocBuilder<SitterBloc, SitterState>(builder: (context, state) {
      if (state is SittersLoaded) {
        return ListView.builder(
          itemCount: state.siters.length,
          itemBuilder: (_, index) {
            return buildSitter(state.siters[index]);
          },
        );
      } else if (state is SittersLoading) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      } else {
        return const Center(
          child: CircularProgressIndicator(),
        );
      }
    });

    return sinbloc;
  }
}
