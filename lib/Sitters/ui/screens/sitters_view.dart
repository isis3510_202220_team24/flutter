import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:petsbnb/Sitters/repository/sitter_repo.dart';
import 'package:petsbnb/Sitters/ui/widgets/sitters_list.dart';

import '../../bloc/bloc/sitter_bloc.dart';

class SittersView extends StatefulWidget {
  const SittersView({super.key});

  @override
  State<SittersView> createState() => _SittersViewState();
}

class _SittersViewState extends State<SittersView> {
  late StreamSubscription internetSubscription;
  bool hasInternet = true;
  @override
  Widget build(BuildContext context) {
    if (hasInternet) {
      return Scaffold(
        appBar: AppBar(title: Text('Sitters')),
        body: MultiBlocProvider(providers: [
          BlocProvider(
            create: (_) => SitterBloc(
              sitterRepository: SitterRepository(),
            )..add(GetSitters()),
          )
        ], child: SittersList()),
      );
    } else {
      return Scaffold(
        appBar: AppBar(title: Text('Sitters')),
        body: Center(
          child: Text("No internet conection"),
        ),
      );
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    internetSubscription =
        InternetConnectionChecker().onStatusChange.listen((status) {
      final hasInternet = status == InternetConnectionStatus.connected;
      setState(() => this.hasInternet = hasInternet);
    });
  }

  @override
  void dispose() {
    internetSubscription.cancel();
    // TODO: implement dispose
    super.dispose();
  }
}
