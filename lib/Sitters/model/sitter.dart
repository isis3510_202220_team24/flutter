import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:petsbnb/Sitters/model/types_helper.dart';

class Sitter extends Equatable {
  final String name;
  final num rating;
  final int position;
  final String age;
  final String pictureUrl;
  final String space;

  Sitter(
      {required this.name,
      required this.rating,
      required this.age,
      required this.position,
      required this.space,
      required this.pictureUrl});

  Map<String, dynamic> toJson() => {
        'name': name,
        'rating': rating,
        'age': age,
        'position': position,
        'space': space,
        'pictureUrl': pictureUrl
      };

  static Sitter fromJson(Map<String, dynamic> json) => Sitter(
      name: json['name'],
      rating: TypesHelper.toInt(json['rating']),
      age: json['age'],
      position: json['position'],
      space: json['space'],
      pictureUrl: json['pictureUrl']);

  @override
  // TODO: implement props
  List<Object?> get props => [name, rating, age, position, space, pictureUrl];
}
