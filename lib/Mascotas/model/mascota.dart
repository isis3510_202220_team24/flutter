import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class Pet extends Equatable {
  final String name;
  final String race;
  final String age;
  final bool needs;
  final String photoURL;

  Pet(
      {required this.name,
      required this.race,
      required this.age,
      required this.needs,
      required this.photoURL});

  Map<String, dynamic> toJson() =>
      {'name': name, 'race': race, 'age': age, 'needs': needs};

  static Pet fromJson(Map<String, dynamic> json) => Pet(
      name: json['name'],
      race: json['race'],
      age: json['age'],
      needs: json['needs'],
      photoURL: json['photoURL']);

  static Pet fromSnapshot(DocumentSnapshot snap) {
    Pet pet = Pet(
        name: snap['name'],
        race: snap['race'],
        age: snap['age'],
        needs: snap['needs'],
        photoURL: snap['photoURL']);
    return pet;
  }

  @override
  // TODO: implement props
  List<Object?> get props => [name, race, age, needs, photoURL];
}
