import 'dart:async';
import 'dart:io';
import 'dart:isolate';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_isolate/flutter_isolate.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:image_picker/image_picker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:path_provider/path_provider.dart';
import 'package:petsbnb/Mascotas/storage/storage_servoce.dart';

class AddMascotaForm extends StatefulWidget {
  const AddMascotaForm({super.key});

  @override
  State<AddMascotaForm> createState() => _AddMascotaFormState();
}

class _AddMascotaFormState extends State<AddMascotaForm> {
  String pathdummy = 'assets/images/dummy.jpg';
  String img2 = 'https://definicion.de/wp-content/uploads/2013/03/perro-1.jpg';
  File? imageFile;
  String? nameValue;
  String? ageValue;
  dynamic raceValue;
  bool needsValue = false;
  bool hasInternet = false;

  TextEditingController? nameController;
  TextEditingController? ageController;
  TextEditingController? raceController;

  final formKey = GlobalKey<FormState>();

  Storage storage = new Storage();
  String path = '';
  String nameFile = '';

  final _myBoxPets = Hive.box('myBoxPets');

  Widget _imageContainer() {
    if (imageFile != null) {
      return Container(
        width: 200,
        height: 300,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.grey,
          image:
              DecorationImage(image: FileImage(imageFile!), fit: BoxFit.fill),
          border: Border.all(width: 8, color: Colors.black12),
          borderRadius: BorderRadius.circular(12.0),
        ),
      );
    } else {
      return Container(
        width: 200,
        height: 300,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.grey,
          image:
              DecorationImage(image: AssetImage(pathdummy), fit: BoxFit.fill),
          border: Border.all(width: 8, color: Colors.black12),
          borderRadius: BorderRadius.circular(12.0),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add pet"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
            key: formKey,
            child: ListView(
              children: <Widget>[
                _imageContainer(),
                TextFormField(
                  controller: nameController,
                  decoration: InputDecoration(labelText: 'Name'),
                  onSaved: (value) {
                    nameValue = value;
                  },
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Llene este campo';
                    }
                  },
                  maxLength: 8,
                ),
                DropdownButtonFormField(
                  decoration: InputDecoration(labelText: 'Race'),
                  items: ['Dog', 'Cat', 'Other'].map<DropdownMenuItem<String>>(
                    (String val) {
                      return DropdownMenuItem(
                        child: Text(val),
                        value: val,
                      );
                    },
                  ).toList(),
                  onChanged: (val) {
                    setState(() {
                      raceValue = val;
                    });
                  },
                  onSaved: (value) {
                    raceValue = value;
                  },
                  validator: (value) => value == null ? 'field required' : null,
                ),
                TextFormField(
                  controller: ageController,
                  keyboardType: TextInputType.number,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  decoration: InputDecoration(labelText: 'Age'),
                  onSaved: (value) {
                    ageValue = value;
                  },
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Llene este campo';
                    }
                  },
                  maxLength: 2,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text("Select Image"),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(primary: Colors.grey),
                        onPressed: () => getImage(source: ImageSource.camera),
                        child: Text('Capture Image')),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(primary: Colors.grey),
                        onPressed: () => getImage(source: ImageSource.gallery),
                        child: Text('Select Image'))
                  ],
                ),
                SwitchListTile(
                  value: needsValue,
                  title: Text('Does your pet has special needs?'),
                  onChanged: (val) {
                    setState(() {
                      needsValue = val;
                    });
                  },
                ),
                ElevatedButton(
                    child: Text('Create new pet'),
                    style: ElevatedButton.styleFrom(
                        primary: Color.fromRGBO(255, 87, 87, 1)),
                    onPressed: () async {
                      hasInternet =
                          await InternetConnectionChecker().hasConnection;
                      final text = hasInternet ? 'Internet' : 'No Internet';
                      final color = hasInternet ? Colors.green : Colors.red;
                      if (hasInternet) {
                        onPressed(context);
                        print('conec');
                      } else {
                        // showSimpleNotification(
                        //     Text(
                        //       '$text',
                        //       style:
                        //           TextStyle(color: Colors.white, fontSize: 20),
                        //     ),
                        //     background: color);
                        final snack =
                            SnackBar(content: Text('No internet connection'));
                        ScaffoldMessenger.of(context)..showSnackBar(snack);
                        print(' no conec');
                      }
                    }),
              ],
            )),
      ),
    );
  }

  onPressed(BuildContext context) async {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      /* print("init Isolate Pets");
      final recievePort = ReceivePort();
      await Isolate.spawn(isolateCreatePets, recievePort.sendPort);
      recievePort.listen((message) => print(message)); */
      createPet(
              name: nameValue,
              age: ageValue,
              race: raceValue,
              img: img2,
              needs: needsValue)
          .then((value) => {print("pet create done")});
      //storage.uploadFile(path, nameFile);
      writeData();
      readData();
      deleteData();
      Navigator.of(context).pop();
    }
  }

  void getImage({required ImageSource source}) async {
    final file = await ImagePicker().pickImage(
        source: source, maxWidth: 640, maxHeight: 480, imageQuality: 80);
    if (file?.path != null) {
      setState(() {
        imageFile = File(file!.path);
        path = file.path;
        nameFile = file.name;
      });
    }
  }

  Future createPet(
      {required String? name,
      required String? age,
      required String? race,
      required String? img,
      required bool needs}) async {
    final docPet = FirebaseFirestore.instance.collection('pets').doc();
    final json = {
      'name': name,
      'age': age,
      'race': race,
      'needs': needs,
      'photoURL': img,
    };

    await docPet.set(json);
  }

  void isolateCreatePets(SendPort sendPort) {
    getTemporaryDirectory().then((dir) {
      print("Isolate Pets temporary directory: $dir");
      createPet(
          name: nameValue,
          age: ageValue,
          race: raceValue,
          img: img2,
          needs: needsValue);
    });
    sendPort.send('$nameValue $ageValue');
  }

  void writeData() {
    _myBoxPets
        .put(1, {'name': nameValue, 'age': ageValue, 'needs': needsValue});
  }

  void readData() {
    print(_myBoxPets.get(1));
  }

  void deleteData() {
    _myBoxPets.delete(1);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    nameController = TextEditingController();
    ageController = TextEditingController();
    raceController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    this.nameController?.dispose();
    this.ageController?.dispose();
    this.raceController?.dispose();
  }
}
