import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/mascotas_bloc.dart';
import '../../repository/pet_repo.dart';
import '../widgets/add_floating_action_button.dart';
import '../widgets/mascotas_list.dart';

class MascotasView extends StatelessWidget {
  const MascotasView({super.key});

  @override
  Widget build(BuildContext context) {
    Widget _addButton() {
      return Container(
        margin: EdgeInsets.all(10),
        alignment: Alignment.bottomRight,
        child: AddFloatingActionButton(),
      );
    }

    return Scaffold(
      appBar: AppBar(title: Text('Pets')),
      body: MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (_) => MascotasBloc(
                petRepository: PetRepository(),
              )..add(GetMascotas()),
            )
          ],
          child: Stack(
            children: <Widget>[MascotasList(), _addButton()],
          )),
    );
  }
}
