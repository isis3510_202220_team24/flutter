import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

class MascotaCard extends StatelessWidget {
  String name;
  String age;
  String race;
  String img;

  MascotaCard(
      {super.key,
      required this.age,
      required this.name,
      required this.race,
      required this.img});

  @override
  Widget build(BuildContext context) {
    final customCacheManager = CacheManager(
      Config(
        'customCacheKeyP',
        stalePeriod: const Duration(days: 15),
        maxNrOfCacheObjects: 100,
      ),
    );
    final card = Container(
      margin: EdgeInsets.all(10),
      child: Stack(children: [
        Container(
          decoration: BoxDecoration(
              image:
                  DecorationImage(fit: BoxFit.cover, image: NetworkImage(img)),
              borderRadius: BorderRadius.all(Radius.circular(20.0)),
              shape: BoxShape.rectangle,
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.black,
                    blurRadius: 10.0,
                    offset: Offset(0.7, 0.9))
              ]),
        ),
        Container(
          height: 25,
          width: 150,
          margin: EdgeInsets.only(right: 10, left: 10, top: 130),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: Color.fromRGBO(255, 87, 87, 1)),
          child: Text(
            name,
            style: TextStyle(
                color: Color.fromARGB(255, 255, 255, 255),
                fontSize: 14,
                fontWeight: FontWeight.bold),
          ),
        )
      ]),
    );
    Widget _cardMascota() {
      return Card(
          elevation: 5,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          child: Container(
            height: 1000,
            child: Column(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      topLeft: Radius.circular(20)),
                  child: CachedNetworkImage(
                    key: UniqueKey(),
                    imageUrl: img,
                    cacheManager: customCacheManager,
                    fit: BoxFit.fill,
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(5.0),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Text(name, textAlign: TextAlign.left),
                        Text(race, textAlign: TextAlign.left),
                        Text(age, textAlign: TextAlign.left)
                      ]),
                )
              ],
            ),
          ));
    }

    if (img != null) {
      return card;
    } else {
      return Container();
    }
  }
}
