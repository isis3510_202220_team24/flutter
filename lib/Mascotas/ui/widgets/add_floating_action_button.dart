import 'package:flutter/material.dart';

class AddFloatingActionButton extends StatefulWidget {
  AddFloatingActionButton({super.key});

  @override
  State<AddFloatingActionButton> createState() =>
      _AddFloatingActionButtonState();
}

class _AddFloatingActionButtonState extends State<AddFloatingActionButton> {
  @override
  Widget build(BuildContext context) {
    Widget _addButton() {
      return FloatingActionButton.extended(
        onPressed: () {
          print('pressed');
          onPressed(context);
        },
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(120)),
        icon: Icon(Icons.add),
        label: Text('Add pet'),
        foregroundColor: Colors.white,
        backgroundColor: Color.fromRGBO(255, 87, 87, 1),
      );
    }

    return _addButton();
  }

  void onPressed(BuildContext context) {
    Navigator.of(context).pushNamed("/addform");
  }
}
