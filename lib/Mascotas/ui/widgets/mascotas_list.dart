import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:petsbnb/Mascotas/bloc/mascotas_bloc.dart';

import '../../model/mascota.dart';
import 'mascota_card.dart';

class MascotasList extends StatelessWidget {
  const MascotasList({super.key});

  @override
  Widget build(BuildContext context) {
    String img1 =
        'https://d7lju56vlbdri.cloudfront.net/var/ezwebin_site/storage/images/_aliases/img_1col/reportajes/y-si-tu-perro-pudiera-vivir-cien-anos/9656705-1-esl-MX/Y-si-tu-perro-pudiera-vivir-cien-anos.jpg';

    String img2 =
        'https://definicion.de/wp-content/uploads/2013/03/perro-1.jpg';

    String img3 =
        'https://www.purina-latam.com/sites/g/files/auxxlc391/files/styles/kraken_generic_max_width_960/public/01_%C2%BFQu%C3%A9-puedo-hacer-si-mi-gato-est%C3%A1-triste-.png?itok=cOA5aYW-';

    Widget buildPet(Pet pet) => MascotaCard(
        age: pet.age, name: pet.name, race: pet.race, img: pet.photoURL);

    return BlocBuilder<MascotasBloc, MascotasState>(
      builder: (context, state) {
        if (state is MascotasLoaded) {
          return GridView.count(
            crossAxisCount: 2,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20,
            children: state.pets.map(buildPet).toList(),
          );
        } else if (state is MascotasLoading) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}
