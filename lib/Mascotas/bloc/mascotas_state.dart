part of 'mascotas_bloc.dart';

abstract class MascotasState extends Equatable {
  const MascotasState();

  @override
  List<Object> get props => [];
}

class MascotasInitial extends MascotasState {
  @override
  List<Object> get props => [];
}

class MascotasCreate extends MascotasState {}

class MascotasError extends MascotasState {
  final String error;
  MascotasError(this.error);
  @override
  List<Object> get props => [error];
}

class MascotasLoading extends MascotasState {
  @override
  List<Object> get props => [];
}

class MascotasLoaded extends MascotasState {
  final List<Pet> pets;
  MascotasLoaded(this.pets);
  @override
  List<Object> get props => [pets];
}
