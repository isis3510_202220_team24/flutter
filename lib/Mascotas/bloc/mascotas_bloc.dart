import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:petsbnb/Mascotas/model/mascota.dart';
import 'package:petsbnb/Mascotas/repository/pet_repo.dart';

part 'mascotas_event.dart';
part 'mascotas_state.dart';

class MascotasBloc extends Bloc<MascotasEvent, MascotasState> {
  final PetRepository petRepository;
  MascotasBloc({required this.petRepository}) : super(MascotasInitial()) {
    on<GetMascotas>(((event, emit) async {
      try {
        List<Pet> pets = await petRepository.get();
        emit(MascotasLoaded(pets));
      } catch (e) {
        emit(MascotasError("error"));
      }
    }));
  }
}
