part of 'mascotas_bloc.dart';

abstract class MascotasEvent extends Equatable {
  const MascotasEvent();

  @override
  List<Object> get props => [];
}

class LoadMascotas extends MascotasEvent {}

class GetMascotas extends MascotasEvent {
  List<Object> get props => [];
}
