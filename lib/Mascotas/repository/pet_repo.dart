import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:petsbnb/Mascotas/model/mascota.dart';

class PetRepository {
  final _fireCloud = FirebaseFirestore.instance.collection('pets');

  Future<List<Pet>> get() async {
    List<Pet> petList = [];
    try {
      final pets = await _fireCloud.get();
      pets.docs.forEach((element) {
        return petList.add(Pet.fromJson(element.data()));
      });
    } on FirebaseException catch (e) {
      if (kDebugMode) {
        print('Failed with error ${e.code}: ${e.message}');
        return petList;
      }
    } catch (e) {
      print("error en getPet " + e.toString());
      throw Exception(e.toString());
    }
    return petList;
  }

  Stream<List<Pet>> getAllPets() {
    return _fireCloud.snapshots().map((snapshot) {
      return snapshot.docs.map((doc) => Pet.fromSnapshot(doc)).toList();
    });
  }
}
