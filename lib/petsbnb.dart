import 'package:flutter/material.dart';
import 'package:petsbnb/views/mapa.dart';
import 'package:petsbnb/Sitters/ui/screens/sitters_view.dart';
import 'package:petsbnb/Sitters/ui/widgets/sitter_card.dart';
import 'package:petsbnb/views/profile_screen.dart';
import 'package:petsbnb/views/main_screen.dart';

import 'Mascotas/ui/screens/mascotas_view.dart';
import 'views/search_view.dart';

class PetsBNBNav extends StatefulWidget {
  const PetsBNBNav({super.key});

  @override
  State<PetsBNBNav> createState() => _PetsBNBNavState();
}

class _PetsBNBNavState extends State<PetsBNBNav> {
  int indexTap = 0;
  final List<Widget> widgetsChildren = [
    MainScreen(),
    //MapScreen(),
    SearchView(),
    const ProfileScreen(),
  ];

  void onTapTapped(int index) {
    setState(() {
      indexTap = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: widgetsChildren[indexTap],
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
            canvasColor: Colors.white,
            primaryColor: Color.fromRGBO(255, 87, 87, 1)),
        child: BottomNavigationBar(
            onTap: onTapTapped,
            currentIndex: indexTap,
            items: [
              BottomNavigationBarItem(icon: Icon(Icons.home), label: ""),
              BottomNavigationBarItem(icon: Icon(Icons.search), label: ""),
              BottomNavigationBarItem(icon: Icon(Icons.person), label: ""),
            ]),
      ),
    );
  }
}
