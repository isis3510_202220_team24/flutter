import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_storage/get_storage.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:petsbnb/reserves/ui/screens/reserve_form.dart';
import 'package:petsbnb/views/initial_screen.dart';
import 'package:petsbnb/views/login_screen.dart';
import 'package:petsbnb/views/place_description.dart';
import 'package:petsbnb/views/register_screen.dart';
import 'package:petsbnb/Mascotas/bloc/mascotas_bloc.dart';
import 'package:petsbnb/Mascotas/repository/pet_repo.dart';
import 'package:petsbnb/Mascotas/ui/screens/mascotas_view.dart';
import 'package:petsbnb/Sitters/bloc/bloc/sitter_bloc.dart';
import 'package:petsbnb/Sitters/repository/sitter_repo.dart';
import 'package:petsbnb/Sitters/ui/screens/sitters_view.dart';
import 'services/firebase_service/auth_service.dart';
import 'views/add_place_view.dart';
import 'Mascotas/ui/screens/add_mascota_form.dart';
import 'petsbnb.dart';
import 'views/place_view.dart';
import 'views/search_view.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await GetStorage.init();
  await Hive.initFlutter();
  var boxPet = await Hive.openBox('myBoxPets');

  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => MascotasBloc(
            petRepository: PetRepository(),
          ),
        ),
        BlocProvider(
          create: (context) => SitterBloc(
            sitterRepository: SitterRepository(),
          ),
        ),
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  String? _setInitialRoute() {
    final firebaseUser = auth.getCurrentUser();

    if (firebaseUser != null) {
      return "/nav";
    } else {
      return "/init";
    }
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Petsbnb',
      theme: ThemeData(primarySwatch: Colors.red),
      initialRoute: _setInitialRoute(),
      debugShowCheckedModeBanner: false,
      routes: {
        //"/init" :(BuildContext context) => InitialScreen(),
        "/init": (BuildContext context) => InitialScreen(),
        "/nav": (BuildContext context) => PetsBNBNav(),
        "/pets": (BuildContext context) => MascotasView(),
        "/sitters": (BuildContext context) => SittersView(),
        "/addform": (BuildContext context) => AddMascotaForm(),
        "/reserveform": (BuildContext context) => ReserveForm(),
        "/login": (BuildContext context) => LoginScreen(),
        "/register": (BuildContext context) => RegisterScreen(),
        "/addPlace": (BuildContext context) => AddPlaceView(),
        "/placeView": (BuildContext context) => PlacesListPage(),
        "/searchView": (BuildContext context) => SearchView(),
        "/placeDescriptionn": (BuildContext context) =>
            const PlaceDescription(),
      },
    );
  }
}
