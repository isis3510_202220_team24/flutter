import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geolocator_android/geolocator_android.dart';
import 'package:geolocator_apple/geolocator_apple.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:ui' as ui;
import 'package:http/http.dart' as http;

import '../models/place.dart';
import '../models/time.dart';
import '../services/model_service/place_service.dart';
import '../widgets/boton_crear.dart';
import '../widgets/connectivity.dart';
import 'package:geolocator/geolocator.dart';

class MapScreen2 extends StatefulWidget {
  @override
  _MapScreenState2 createState() => _MapScreenState2();
}

class _MapScreenState2 extends State<MapScreen2> {
  final _firestore = FirebaseFirestore.instance;

  static const _initialCameraPosition = CameraPosition(
    target: LatLng(4.60971, -74.08175),
    zoom: 11.5,
  );

  late GoogleMapController _googleMapController;

  @override
  void dispose() {
    _googleMapController.dispose();
    super.dispose();
  }

  List<Marker> allMarkers = [];
  List<Marker> filteredMarkers = [];
  bool isLoading = false;
  LatLng posActual = const LatLng(4.60971, -74.08175);
  var filterdist;

  @override
  void initState() {
    getData();
    super.initState();
  }

  List<Place> places = [];

  getData() async {
    setState(() {
      isLoading = true;
    });

    bool result = await connectionStatus.getNormalStatus();

    if (result) {
      try {
        final box = GetStorage();

        if (box.read('places') == null) {
          try {
            places = (await placeService.getPlaces());
            _addMarker(places);
            setState(() {
              isLoading = false;
            });
          } catch (e) {
            print(e);
            setState(() {
              isLoading = false;
            });
          }
        } else {
          final List<dynamic> aux = box.read('places');
          print('auuuuux: ${aux.length}');
          aux.forEach((element) {
            places.add(Place(
              id: element.id,
              titulo: element.titulo,
              descripcion: element.descripcion,
              ciudad: element.ciudad,
              barrio: element.barrio,
              ubicacion: element.ubicacion,
              precio: element.precio,
            ));
          });

          _addMarker(places);
          setState(() {
            isLoading = false;
          });
        }
      } on Exception catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      setState(() {
        isLoading = false;
      });
      //CustomSnackBars.showErrorSnackBar("No tienes internet, intentalo más tarde");
      print("No hay internet");
      // set up the AlertDialog
      AlertDialog alert = AlertDialog(
        title: const Text("¡Sin Conexion!"),
        content:
            const Text("Actualmente no tienes internet, revisa tu conexion"),
        actions: [
          CustomButton(
            buttonText: "Home",
            onPressed: () async {
              await Navigator.of(context).pushNamed("/nav");
              getData();
            },
            isLoading: false.obs,
          ),
          CustomButton(
            buttonText: "Reintentar",
            onPressed: () async {
              await Navigator.of(context).pushNamed("/searchView");
              getData();
            },
            isLoading: false.obs,
          ),
        ],
      );

      // show the dialog
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }
  }

  double deg2rad(deg) {
    return deg * (pi / 180);
  }

  double getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    double R = 6371; // Radius of the earth in km
    double dLat = deg2rad(lat2 - lat1); // deg2rad below
    double dLon = deg2rad(lon2 - lon1);
    double a = sin(dLat / 2) * sin(dLat / 2) +
        cos(deg2rad(lat1)) * cos(deg2rad(lat2)) * sin(dLon / 2) * sin(dLon / 2);
    double c = 2 * atan2(sqrt(a), sqrt(1 - a));
    double d = R * c; // Distance in km
    return d;
  }

  Future<void> _addMarker(List<Place> usuarios) async {
    usuarios.forEach((element) async {
      await _determinePosition();
      double distancia = await getDistanceFromLatLonInKm(
          posActual.latitude,
          posActual.longitude,
          element.ubicacion!.latitud!,
          element.ubicacion?.longitud!);

      String dist = "${double.parse((distancia).toStringAsFixed(2))}km";
      Marker _marker = Marker(
        markerId: MarkerId(element.titulo!),
        infoWindow: InfoWindow(
            title: element.titulo,
            snippet: "Precio: ${element.precio}- distancia:$dist"),
        //icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
        //position: LatLng(4.60971, -74.08175),

        position:
            LatLng(element.ubicacion!.latitud!, element.ubicacion!.longitud!),
        //icon: BitmapDescriptor.fromBytes(targetlUinit8List),
      );
      allMarkers.add(_marker);
    });
    setState(() {
      //offer.latitude = pos.latitude;
      //offer.longitude = pos.longitude;
    });
  }

  Future<Position> _determinePosition() async {
    _registerPlatformInstance();
    bool serviceEnabled;
    LocationPermission permission;
    try {
      if (await Geolocator.isLocationServiceEnabled() == false) {
        showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text("Sin GPS"),
              content: const Text("Por favor, activa tu gps"),
              actions: [
                Center(
                  child: CustomButton(
                    buttonText: "Regresar",
                    onPressed: () async {
                      Navigator.pop(context);
                    },
                    isLoading: false.obs,
                  ),
                ),
              ],
            );
          },
        );
        return Future.error('No hay gps');
      }

      serviceEnabled = await Geolocator.isLocationServiceEnabled();

      if (!serviceEnabled) {
        return Future.error('Location services are disabled');
      }

      permission = await Geolocator.checkPermission();

      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();

        if (permission == LocationPermission.denied) {
          // show the dialog
          showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: const Text("Sin permisos :("),
                content: const Text(
                    "No tenemos permisos para saber tu ubicación actual"),
                actions: [
                  Center(
                    child: CustomButton(
                      buttonText: "Regresar",
                      onPressed: () async {
                        Navigator.pop(context);
                      },
                      isLoading: false.obs,
                    ),
                  ),
                ],
              );
            },
          );
          return Future.error("Location permission denied");
        }
      }

      if (permission == LocationPermission.deniedForever) {
        showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: const Text("Sin permisos :("),
                content: const Text(
                    "No tenemos permisos para saber tu ubicación actual"),
                actions: [
                  CustomButton(
                    buttonText: "Regresar",
                    onPressed: () async {
                      Navigator.pop(context);
                    },
                    isLoading: false.obs,
                  ),
                ],
              );
            });
        return Future.error('Location permissions are permanently denied');
      }

      Position position = await Geolocator.getCurrentPosition();

      this.posActual = LatLng(position.latitude, position.longitude);
      return position;
    } catch (e) {
      print('--------------------------------');
      print(e);
      return Future.error('Sinn gps');
    }
  }

  void _registerPlatformInstance() {
    if (Platform.isAndroid) {
      GeolocatorAndroid.registerWith();
    } else if (Platform.isIOS) {
      GeolocatorApple.registerWith();
    }
  }

  Future<void> filterMarkers(List<Place> usuarios, filDist) async {
    filteredMarkers = [];
    usuarios.forEach((element) async {
      print(element.titulo);
      await _determinePosition();
      double distancia = getDistanceFromLatLonInKm(
          posActual.latitude,
          posActual.longitude,
          element.ubicacion!.latitud!,
          element.ubicacion?.longitud!);

      var filDist2 = double.parse(filDist);

      if (distancia <= filDist2) {
        print(distancia - filDist2);
        String dist = "${double.parse((distancia).toStringAsFixed(2))}km";
        Marker _marker = Marker(
            markerId: MarkerId(element.titulo!),
            infoWindow: InfoWindow(
                title: element.titulo,
                snippet: "Precio: ${element.precio}- distancia:$dist"),
            //icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
            //position: LatLng(4.60971, -74.08175),

            position: LatLng(
                element.ubicacion!.latitud!, element.ubicacion!.longitud!),
            onTap: () async {
              //this is what you're looking for!
              await Navigator.of(context).pushNamed("/nav");
            }
            //icon: BitmapDescriptor.fromBytes(targetlUinit8List),
            );
        filteredMarkers.add(_marker);
        print("anadio marcador");
      } else {}
    });
    setState(() {
      //offer.latitude = pos.latitude;
      //offer.longitude = pos.longitude;
      //_googleMapController.dispose();
      allMarkers = filteredMarkers;
    });
  }

  Future getDist() {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title: const Text("Filter by distance"),
            contentPadding: const EdgeInsets.all(10.0),
            content: TextField(
              decoration:
                  const InputDecoration(hintText: "Enter distance in km"),
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.digitsOnly
              ],
              onChanged: (val) {
                setState(() {
                  filterdist = val;
                  filterMarkers(places, filterdist);
                });
              },
            ),
            actions: [
              TextButton(
                onPressed: () async {
                  Navigator.pop(context, true);
                  //filterMarkers(places, filterdist);
                  setState(() {});
                  //await Navigator.of(context).pushNamed("/searchView");
                  //Navigator.of(context).pop();
                },
                child: const Text("Ok"),
                //color: Colors.transparent)
              )
            ],
          );
        });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text("Map View"),
      //   centerTitle: true,
      //   automaticallyImplyLeading: false,
      //   //actions: [
      //   //IconButton(onPressed: () {}, icon: Icon(Icons.search)),
      //   //IconButton(onPressed: () {}, icon: Icon(Icons.filter))
      //   //],
      // ),
      body: Column(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height / 1.85,
            width: MediaQuery.of(context).size.width,
            child: Stack(children: <Widget>[
              GoogleMap(
                //mapType: MapType.hybrid,
                myLocationButtonEnabled: false,
                zoomControlsEnabled: false,
                zoomGesturesEnabled: true,
                initialCameraPosition: _initialCameraPosition,
                markers: Set.from(allMarkers),
                onMapCreated: (controller) => _googleMapController = controller,
              ),
              Stack(
                  // mainAxisAlignment: MainAxisAlignment.end,
                  //crossAxisAlignment: CrossAxisAlignment.end,
                  //alignment: Alignment.bottomRight,
                  children: <Widget>[
                    Align(
                        alignment: Alignment.bottomRight,
                        child: Container(
                            //height: 100,
                            //width: 100,
                            //color: Colors.greenAccent,
                            child: Column(
                          children: [
                            FloatingActionButton(
                                heroTag: "botonFiltrar",
                                backgroundColor: Theme.of(context).primaryColor,
                                foregroundColor: Colors.black,
                                child: const Icon(Icons.filter_alt),
                                onPressed: () async {
                                  final stopwatch = Stopwatch()..start();
                                  Position position =
                                      await _determinePosition();

                                  _googleMapController.animateCamera(
                                      CameraUpdate.newCameraPosition(
                                          CameraPosition(
                                              target: LatLng(position.latitude,
                                                  position.longitude),
                                              zoom: 14)));

                                  allMarkers = [];
                                  await getDist();
                                  //Navigator.of(context).pop(true);
                                  allMarkers.add(Marker(
                                      markerId:
                                          const MarkerId('currentLocation'),
                                      position: LatLng(position.latitude,
                                          position.longitude)));

                                  var elapsed = stopwatch.elapsed;
                                  print(
                                      'Tiempo en determinar ubicacion y filtrar marcadores: ${stopwatch.elapsed}');
                                  Time time = Time(
                                    method: "Filter map",
                                    time: elapsed.inMilliseconds,
                                  );

                                  await recordTime(
                                    time: time.toJson(),
                                  );
                                  setState(() {});
                                }),
                            FloatingActionButton(
                                heroTag: "botonUbicar",
                                backgroundColor: Theme.of(context).primaryColor,
                                foregroundColor: Colors.black,
                                child: const Icon(Icons.my_location),
                                onPressed: () async {
                                  final stopwatch = Stopwatch()..start();
                                  Position position =
                                      await _determinePosition();
                                  await getData();

                                  _googleMapController.animateCamera(
                                      CameraUpdate.newCameraPosition(
                                          CameraPosition(
                                              target: LatLng(position.latitude,
                                                  position.longitude),
                                              zoom: 14)));

                                  //allMarkers = [];
                                  //await getDist();
                                  //Navigator.of(context).pop(true);
                                  allMarkers.add(Marker(
                                      markerId:
                                          const MarkerId('currentLocation'),
                                      icon:
                                          BitmapDescriptor.defaultMarkerWithHue(
                                              BitmapDescriptor.hueAzure),
                                      position: LatLng(position.latitude,
                                          position.longitude)));

                                  var elapsed = stopwatch.elapsed;
                                  print(
                                      'Tiempo en ubicar y poner marcadores: ${stopwatch.elapsed}');
                                  Time time = Time(
                                    method: "Locate map",
                                    time: elapsed.inMilliseconds,
                                  );

                                  await recordTime(
                                    time: time.toJson(),
                                  );
                                  setState(() {});
                                })
                          ],
                        ))),
                    const SizedBox(
                      height: 10,
                    ),
                  ]),
            ]),
          ),
          const SizedBox(
            height: 10.0,
          ),
          Expanded(
            child: isLoading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : places.isNotEmpty
                    ? ListView.builder(
                        itemCount: places.length,
                        itemBuilder: (context, index) {
                          var place = places[index];
                          return Card(
                            child: ListTile(
                              title: Text(places[index].titulo!),
                              subtitle: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                              ),
                              trailing: IconButton(
                                icon: const Icon(Icons.open_in_full),
                                color: Theme.of(context).primaryColor,
                                onPressed: () async {
                                  await Navigator.pushNamed(
                                      context, '/placeDescriptionn',
                                      arguments: {
                                        'place': places[index].toJson()
                                      });
                                },
                              ),
                            ),
                          );
                        },
                      )
                    : const Center(
                        child: Text('No se encontraron places'),
                      ),
          )
        ],
      ),

      //floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Future<bool> recordTime({
    required Map<String, dynamic> time,
  }) async {
    try {
      final CollectionReference collRef = _firestore.collection('times');
      final result = await _firestore.collection('times').add(time);
      final DocumentReference docReferance = collRef.doc(result.id);
      final Time aux = Time.fromJson(time);
      aux.id = result.id;

      await _firestore
          .collection("times")
          .doc(docReferance.id)
          .set({...aux.toJson()});

      return true;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }
}
