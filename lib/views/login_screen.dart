// ignore_for_file: prefer_const_literals_to_create_immutables

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String email = "";
  String password = "";

  @override
  Widget build(BuildContext context) {
    FirebaseAuth.instance.authStateChanges().listen((User? user) {
      if (user != null) {
        //Navigator.of(context).pushNamed("/");
      }
    });
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
      body: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
        Column(children: [
          const Text(
            "Log in",
            style: TextStyle(fontSize: 25, height: 3, color: Colors.black),
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 15,
          ),
          SizedBox(
            width: 280,
            child: TextField(
              onChanged: (value) => email = value,
              // ignore: prefer_const_constructors
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                labelText: 'Email',
              ),
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          SizedBox(
            width: 280,
            child: TextField(
              obscureText: true,
              onChanged: (value) => password = value,
              // ignore: prefer_const_constructors
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                labelText: 'Password',
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          SizedBox(
              width: 150,
              height: 40,
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      //backgroundColor: const Color.fromRGBO(255, 87, 87, 1),
                      shape: RoundedRectangleBorder(
                          //to set border radius to button
                          borderRadius: BorderRadius.circular(120))),
                  onPressed: () {
                    onPressedLogin(context);
                  },
                  child: const Text('Log in'))),
          const SizedBox(
            height: 20,
          ),
          TextButton(
            onPressed: () {
              onPressedRegister(context);
            },
            child: const Text("Sign in"),
          )
        ])
      ]),
    ));
  }

  Future<void> onPressedLogin(BuildContext context) async {
    try {
      if (email == '' || password == '') {
        // ignore: prefer_const_constructors
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: const Text("Try filling the inputs"),
        ));
      } else {
        final credential = await FirebaseAuth.instance
            .signInWithEmailAndPassword(email: email, password: password)
            .then((value) => Navigator.of(context).pushNamed("/nav"));
      }
      // ignore: use_build_context_synchronously
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        // ignore: prefer_const_constructors
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: const Text("No user found for that email"),
        ));
        print('No user found for that email.');
      } else if (e.code == 'wrong-password') {
        // ignore: prefer_const_constructors
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: const Text("Wrong password provided for that userl"),
        ));
        print('Wrong password provided for that user.');
      }
    }
  }

  void onPressedRegister(BuildContext context) {
    Navigator.of(context).pushNamed("/register");
  }
}
