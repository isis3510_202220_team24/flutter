import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import '../models/place.dart';

class PlaceDescription extends StatefulWidget {
  const PlaceDescription({super.key});

  @override
  State<PlaceDescription> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<PlaceDescription> {
  @override
  Widget build(BuildContext context) {
    final arguments = (ModalRoute.of(context)?.settings.arguments ??
        <String, dynamic>{}) as Map;

    final Place place = Place.fromJson(arguments['place']);

    return Scaffold(
      appBar: AppBar(
        title: Text(place.titulo!),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 30),
              CarouselSlider.builder(
                itemCount: place.imagenes!.length,
                itemBuilder: (
                  BuildContext context,
                  int itemIndex,
                  int pageViewIndex,
                ) {
                  return SizedBox(
                    width: 200,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: CachedNetworkImage(
                        imageUrl: place.imagenes![itemIndex].url!,
                        fit: BoxFit.cover,
                      ),
                    ),
                  );
                },
                options: CarouselOptions(height: 200, viewportFraction: 0.6),
              ),
              const SizedBox(height: 20),
              titleAndSubtitle(
                title: 'Descripción',
                subtitle: place.descripcion!,
              ),
              titleAndSubtitle(
                title: 'Ciudad',
                subtitle: place.ciudad!,
              ),
              titleAndSubtitle(
                title: 'Barrio',
                subtitle: place.barrio!,
              ),
              titleAndSubtitle(
                title: 'Precio',
                subtitle: '${place.precio}',
              ),
            ],
          ),
        ),
      ),
    );
  }

  titleAndSubtitle({
    required String title,
    required String subtitle,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: const TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 18,
            ),
            textAlign: TextAlign.start,
          ),
          const SizedBox(height: 10),
          Text(
            subtitle,
            style: const TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 16,
            ),
          ),
        ],
      ),
    );
  }
}
