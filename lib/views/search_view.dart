import 'package:flutter/material.dart';
import 'package:petsbnb/views/mapa2.dart';
import 'package:provider/provider.dart';

import 'mapa.dart';
import 'place_view.dart';
import 'place_view2.dart';

class SearchView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SearchView();
  }
}

// ignore: non_constant_identifier_names
class _SearchView extends State<SearchView> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(50.0),
            child: AppBar(
              bottom: TabBar(
                indicator: BoxDecoration(
                    borderRadius: BorderRadius.circular(50), // Creates border
                    //color: Color.fromARGB(255, 21, 129, 96)), //Change background color from here
                    color: Theme.of(context).primaryColor),
                tabs: [Tab(icon: Icon(Icons.map)), Tab(icon: Icon(Icons.list))],
              ),
            )),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: [
            MapScreen2(),
            PlacesListPage2(),
            // ListGroups(),
            //Icon(Icons.flight, size: 350),
          ],
        ),
      ),
    );
  }
}
