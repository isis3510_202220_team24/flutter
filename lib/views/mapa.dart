import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geolocator_android/geolocator_android.dart';
import 'package:geolocator_apple/geolocator_apple.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:ui' as ui;
import 'package:http/http.dart' as http;

import '../models/place.dart';
import '../models/time.dart';
import '../services/model_service/place_service.dart';
import '../widgets/boton_crear.dart';
import '../widgets/connectivity.dart';

class MapScreen extends StatefulWidget {
  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  final _firestore = FirebaseFirestore.instance;
  Future<bool> recordTime({
    required Map<String, dynamic> time,
  }) async {
    try {
      final CollectionReference collRef = _firestore.collection('times');
      final result = await _firestore.collection('times').add(time);
      final DocumentReference docReferance = collRef.doc(result.id);
      final Time aux = Time.fromJson(time);
      aux.id = result.id;

      await _firestore
          .collection("times")
          .doc(docReferance.id)
          .set({...aux.toJson()});

      return true;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  @override
  static const _initialCameraPosition = CameraPosition(
    target: LatLng(4.60971, -74.08175),
    zoom: 11.5,
  );

  late GoogleMapController _googleMapController;
  void dispose() {
    _googleMapController.dispose();
    super.dispose();
  }

  List<Marker> allMarkers = [];
  List<Marker> filteredMarkers = [];
  bool isLoading = false;
  LatLng posActual = LatLng(4.60971, -74.08175);
  var filterdist;

  @override
  void initState() {
    getData();
    super.initState();
  }

  List<Place> places = [];

  getData() async {
    setState(() {
      isLoading = true;
    });

    bool result = await connectionStatus.getNormalStatus();

    if (result) {
      try {
        places = (await placeService.getPlaces())!;
        _addMarker(places);
        setState(() {
          isLoading = false;
        });
      } on Exception catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      setState(() {
        isLoading = false;
      });
      //CustomSnackBars.showErrorSnackBar("No tienes internet, intentalo más tarde");
      print("No hay internet");
      // set up the AlertDialog
      AlertDialog alert = AlertDialog(
        title: Text("¡Sin Conexion!"),
        content: Text("Actualmente no tienes internet, revisa tu conexion"),
        actions: [
          CustomButton(
            buttonText: "Home",
            onPressed: () async {
              await Navigator.of(context).pushNamed("/nav");
              getData();
            },
            isLoading: false.obs,
          ),
          CustomButton(
            buttonText: "Reintentar",
            onPressed: () async {
              await Navigator.of(context).pushNamed("/searchView");
              getData();
            },
            isLoading: false.obs,
          ),
        ],
      );

      // show the dialog
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }
  }

  double deg2rad(deg) {
    return deg * (pi / 180);
  }

  double getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    double R = 6371; // Radius of the earth in km
    double dLat = deg2rad(lat2 - lat1); // deg2rad below
    double dLon = deg2rad(lon2 - lon1);
    double a = sin(dLat / 2) * sin(dLat / 2) +
        cos(deg2rad(lat1)) * cos(deg2rad(lat2)) * sin(dLon / 2) * sin(dLon / 2);
    double c = 2 * atan2(sqrt(a), sqrt(1 - a));
    double d = R * c; // Distance in km
    return d;
  }

  Future<void> _addMarker(List<Place> usuarios) async {
    usuarios.forEach((element) async {
      await _determinePosition();
      double distancia = await getDistanceFromLatLonInKm(
          posActual.latitude,
          posActual.longitude,
          element.ubicacion!.latitud!,
          element.ubicacion?.longitud!);

      String dist =
          double.parse((distancia).toStringAsFixed(2)).toString() + "km";
      Marker _marker = Marker(
        markerId: MarkerId(element.titulo!),
        infoWindow: InfoWindow(
            title: element.titulo,
            snippet:
                "Precio: " + element.precio.toString() + "- distancia:" + dist),
        //icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
        //position: LatLng(4.60971, -74.08175),

        position:
            LatLng(element.ubicacion!.latitud!, element.ubicacion!.longitud!),
        //icon: BitmapDescriptor.fromBytes(targetlUinit8List),
      );
      allMarkers.add(_marker);
    });
    setState(() {
      //offer.latitude = pos.latitude;
      //offer.longitude = pos.longitude;
    });
  }

  Future<Position> _determinePosition() async {
    _registerPlatformInstance();
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();

    if (!serviceEnabled) {
      return Future.error('Location services are disabled');
    }

    permission = await Geolocator.checkPermission();

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();

      if (permission == LocationPermission.denied) {
        return Future.error("Location permission denied");
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error('Location permissions are permanently denied');
    }

    Position position = await Geolocator.getCurrentPosition();

    this.posActual = LatLng(position.latitude, position.longitude);
    return position;
  }

  void _registerPlatformInstance() {
    if (Platform.isAndroid) {
      GeolocatorAndroid.registerWith();
    } else if (Platform.isIOS) {
      GeolocatorApple.registerWith();
    }
  }

  Future<void> filterMarkers(List<Place> usuarios, filDist) async {
    filteredMarkers = [];
    usuarios.forEach((element) async {
      print(element.titulo);
      await _determinePosition();
      double distancia = getDistanceFromLatLonInKm(
          posActual.latitude,
          posActual.longitude,
          element.ubicacion!.latitud!,
          element.ubicacion?.longitud!);

      var filDist2 = double.parse(filDist);

      if (distancia <= filDist2) {
        print(distancia - filDist2);
        String dist =
            double.parse((distancia).toStringAsFixed(2)).toString() + "km";
        Marker _marker = Marker(
            markerId: MarkerId(element.titulo!),
            infoWindow: InfoWindow(
                title: element.titulo,
                snippet: "Precio: " +
                    element.precio.toString() +
                    "- distancia:" +
                    dist),
            //icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
            //position: LatLng(4.60971, -74.08175),

            position: LatLng(
                element.ubicacion!.latitud!, element.ubicacion!.longitud!),
            onTap: () async {
              //this is what you're looking for!
              await Navigator.of(context).pushNamed("/nav");
            }
            //icon: BitmapDescriptor.fromBytes(targetlUinit8List),
            );
        filteredMarkers.add(_marker);
        print("anadio marcador");
      } else {}
    });
    setState(() {
      //offer.latitude = pos.latitude;
      //offer.longitude = pos.longitude;
      //_googleMapController.dispose();
      allMarkers = filteredMarkers;
    });
  }

  Future getDist() {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title: Text("Filter by distance"),
            contentPadding: EdgeInsets.all(10.0),
            content: TextField(
              decoration: InputDecoration(hintText: "Enter distance in km"),
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.digitsOnly
              ],
              onChanged: (val) {
                setState(() {
                  filterdist = val;
                  filterMarkers(places, filterdist);
                });
              },
            ),
            actions: [
              TextButton(
                onPressed: () async {
                  Navigator.pop(context, true);
                  //filterMarkers(places, filterdist);
                  setState(() {});
                  //await Navigator.of(context).pushNamed("/searchView");
                  //Navigator.of(context).pop();
                },
                child: Text("Ok"),
                //color: Colors.transparent)
              )
            ],
          );
        });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Map View"),
        centerTitle: true,
        automaticallyImplyLeading: false,
        //actions: [
        //IconButton(onPressed: () {}, icon: Icon(Icons.search)),
        //IconButton(onPressed: () {}, icon: Icon(Icons.filter))
        //],
      ),
      body: GoogleMap(
        //mapType: MapType.hybrid,
        myLocationButtonEnabled: false,
        zoomControlsEnabled: false,
        zoomGesturesEnabled: true,
        initialCameraPosition: _initialCameraPosition,
        markers: Set.from(allMarkers),
        onMapCreated: (controller) => _googleMapController = controller,
      ),
      floatingActionButton:
          Column(mainAxisAlignment: MainAxisAlignment.end, children: [
        FloatingActionButton(
            backgroundColor: Theme.of(context).primaryColor,
            foregroundColor: Colors.black,
            child: const Icon(Icons.filter_alt),
            onPressed: () async {
              final stopwatch = Stopwatch()..start();
              Position position = await _determinePosition();

              _googleMapController.animateCamera(CameraUpdate.newCameraPosition(
                  CameraPosition(
                      target: LatLng(position.latitude, position.longitude),
                      zoom: 14)));

              allMarkers = [];
              await getDist();
              //Navigator.of(context).pop(true);
              allMarkers.add(Marker(
                  markerId: const MarkerId('currentLocation'),
                  position: LatLng(position.latitude, position.longitude)));

              var elapsed = stopwatch.elapsed;
              print(
                  'Tiempo en determinar ubicacion y filtrar marcadores: ${stopwatch.elapsed}');
              Time time = Time(
                method: "Filter map",
                time: elapsed.inMilliseconds,
              );

              await recordTime(
                time: time.toJson(),
              );
              setState(() {});
            }),
        SizedBox(
          height: 10,
        ),
        FloatingActionButton(
            backgroundColor: Theme.of(context).primaryColor,
            foregroundColor: Colors.black,
            child: const Icon(Icons.my_location),
            onPressed: () async {
              final stopwatch = Stopwatch()..start();
              Position position = await _determinePosition();
              await getData();

              _googleMapController.animateCamera(CameraUpdate.newCameraPosition(
                  CameraPosition(
                      target: LatLng(position.latitude, position.longitude),
                      zoom: 14)));

              //allMarkers = [];
              //await getDist();
              //Navigator.of(context).pop(true);
              allMarkers.add(Marker(
                  markerId: const MarkerId('currentLocation'),
                  position: LatLng(position.latitude, position.longitude)));

              var elapsed = stopwatch.elapsed;
              print(
                  'Tiempo en ubicar y poner marcadores: ${stopwatch.elapsed}');
              Time time = Time(
                method: "Locate map",
                time: elapsed.inMilliseconds,
              );

              await recordTime(
                time: time.toJson(),
              );
              setState(() {});
            })
      ]),

      //floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
