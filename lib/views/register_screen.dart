// ignore_for_file: prefer_const_literals_to_create_immutables

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  String email = "";
  String password = "";

  @override
  Widget build(BuildContext context) {
    FirebaseAuth.instance.authStateChanges().listen((User? user) {
      if (user != null) {
        //Navigator.of(context).pushNamed("/");
      }
    });
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
      body: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
        Column(children: [
          const Text("Sign in",
              style: TextStyle(fontSize: 25, height: 3, color: Colors.black),
              textAlign: TextAlign.center),
          const SizedBox(
            height: 15,
          ),
          SizedBox(
            width: 280,
            child: TextField(
              onChanged: (value) => email = value,
              // ignore: prefer_const_constructors
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                labelText: 'Email',
              ),
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          SizedBox(
            width: 280,
            child: TextField(
              obscureText: true,
              onChanged: (value) => password = value,
              // ignore: prefer_const_constructors
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                labelText: 'Password',
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
              width: 150,
              height: 40,
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      //backgroundColor: const Color.fromRGBO(255, 87, 87, 1),
                      shape: RoundedRectangleBorder(
                          //to set border radius to button
                          borderRadius: BorderRadius.circular(30))),
                  onPressed: () {
                    onPressedRegister(context);
                  },
                  child: const Text('Sign in'))),
          const SizedBox(
            height: 20,
          ),
          Container(
            child: TextButton(
              onPressed: () {
                onPressedLogin(context);
              },
              child: Text("Log in"),
            ),
          )
        ])
      ]),
    ));
  }

  Future<void> onPressedRegister(BuildContext context) async {
    try {
      if (email == '' || password == '') {
        // ignore: prefer_const_constructors
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: const Text("Try filling the inputs"),
        ));
      } else {
        final credential = await FirebaseAuth.instance
            .createUserWithEmailAndPassword(
              email: email,
              password: password,
            )
            .then((value) => Navigator.of(context).pushNamed("/login"));
      }
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
        // ignore: prefer_const_constructors
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: const Text("The password provided is too weak"),
        ));
      } else if (e.code == 'email-already-in-use') {
        // ignore: prefer_const_constructors
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: const Text("The account already exists for that email"),
        ));
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }
  }

  void onPressedLogin(BuildContext context) {
    Navigator.of(context).pushNamed("/login");
  }
}
