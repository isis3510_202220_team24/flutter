// ignore_for_file: prefer_const_literals_to_create_immutables

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_isolate/flutter_isolate.dart';
import 'package:get_storage/get_storage.dart';
import 'package:path_provider/path_provider.dart';
import 'package:petsbnb/models/place.dart';
import 'package:petsbnb/services/model_service/place_service.dart';

@pragma('vm:entry-point')
void isolatePlaces(String arg) async {
  getTemporaryDirectory().then((dir) async {
    print("Isolate PLACES temporary directory: $dir");

    List<dynamic> places = [];
    places = await placeService.getPlaces();
    final box = GetStorage();
    box.write('places', places);
  });
  Timer.periodic(
    const Duration(seconds: 1),
    (timer) {
      print("Isolate PLACES corriendo");
    },
  );
}

class InitialScreen extends StatefulWidget {
  const InitialScreen({Key? key}) : super(key: key);

  @override
  State<InitialScreen> createState() => _InitialScreenState();
}

class _InitialScreenState extends State<InitialScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: const Color.fromRGBO(218, 226, 235, 1),
            body: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/init.png'),
                  fit: BoxFit.cover,
                ),
              ),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    const Text("petsbnb",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 25,
                            height: 3,
                            color: Color.fromRGBO(175, 188, 203, 1)),
                        textAlign: TextAlign.center),
                    const Text(
                      "Looking out your pet",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 35,
                          height: 1.5,
                          color: Color.fromRGBO(255, 87, 87, 1)),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                        alignment: Alignment.bottomCenter,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              //backgroundColor: const Color.fromRGBO(255, 87, 87, 1),
                              shape: RoundedRectangleBorder(
                                  //to set border radius to button
                                  borderRadius: BorderRadius.circular(30)),
                            ),
                            onPressed: () {
                              onPressed(context);
                            },
                            child: const Text('Get started')))
                  ]),
            )));
  }

  void onPressed(BuildContext context) async {
    // final box = GetStorage();
    // box.erase();
    Navigator.of(context).pushNamed("/login");
    print(
        "Temp directory in main isolate : ${(await getTemporaryDirectory()).path}");
    final isolate = await FlutterIsolate.spawn(isolatePlaces, "hello");
    // Timer(const Duration(seconds: 5), () {
    //   print("Pausing Isolate 1");
    //   isolate.pause();
    // });
    // Timer(const Duration(seconds: 10), () {
    //   print("Resuming Isolate 1");
    //   isolate.resume();
    // });
    Timer(const Duration(seconds: 20), () {
      print("Killing Isolate places");
      isolate.kill();
      final box = GetStorage();

      List<Place> places = [];
      places = box.read('places') ?? [];
      print('-----------------------------------------------------');
      print(places.length);
    });
  }
}
