import 'package:flutter/material.dart';
import 'package:petsbnb/widgets/main_screen_button.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      alignment: Alignment.topCenter,
      children: [
        Container(
          margin: EdgeInsets.only(top: 100),
          child: Text(
            "Welcome to Pets BNB",
            style: TextStyle(fontSize: 24),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 150, left: 20, right: 20),
          child: Text(
            "start by choosing pets or sitters available",
            style: TextStyle(fontSize: 18, color: Colors.grey),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 150),
          child: GridView.count(
            crossAxisCount: 2,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20,
            children: [
              BigButtonMain(title: 'Pets', route: '/pets'),
              BigButtonMain(title: 'Sitters', route: '/sitters')
            ],
          ),
        ),
      ],
    ));
  }
}
