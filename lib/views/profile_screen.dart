// ignore_for_file: prefer_const_literals_to_create_immutables, unused_local_variable
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/file.dart';
import 'package:get/state_manager.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:petsbnb/models/imagen.dart';
import 'package:petsbnb/services/firebase_service/auth_service.dart';
import 'package:petsbnb/services/model_service/user_model.dart';
import 'package:petsbnb/views/add_place_view.dart';
import 'package:get/get.dart';
import 'package:petsbnb/widgets/boton_crear.dart';
import 'package:petsbnb/widgets/custom_text_field.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  File? file;

  final TextEditingController nameController = TextEditingController();
  final TextEditingController usernameController = TextEditingController();

  String errorText = '';
  RxBool isLoading = false.obs;
  var key = GlobalKey<FormState>();
  bool imageError = false;
  @override
  void initState() {
    Get.testMode = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAuth.instance.authStateChanges().listen((User? user) {
      if (user == null) {
        Navigator.of(context).pushNamed("/login");
      }
    });
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text(
            'Perfil',
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 20,
            ),
          ),
        ),
        backgroundColor: Colors.white,
        body: Form(
          key: key,
          child: SafeArea(
            child: Column(
              children: [
                const SizedBox(
                  height: 40,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    ImageContainer(
                      file: file,
                      onTap: () {
                        pickLogoPicture(true, 1);
                      },
                    )
                  ],
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  errorText,
                  style: const TextStyle(
                    fontSize: 14,
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 40),
                CustomTextField(
                  textEditingController: nameController,
                  titleText: "Nombre",
                ),
                const SizedBox(height: 40),
                CustomTextField(
                  textEditingController: usernameController,
                  titleText: "Username",
                ),
                const SizedBox(height: 30),
                CustomButton(
                  buttonText: 'Guardar',
                  onPressed: () async {
                    isLoading.value = true;
                    if (file != null) {
                      setState(() {
                        errorText = '';
                      });
                    } else {
                      setState(() {
                        errorText = 'Por favor ingresa una imagen';
                      });
                      isLoading.value = false;

                      return;
                    }
                    if (key.currentState!.validate()) {
                      try {
                        print(1);

                        var imagen = Imagen(archivo: file, order: 1);

                        var user = {
                          'name': nameController.text,
                          'username': usernameController.text,
                        };

                        await userService.addUser(
                          user: user,
                          imagen: imagen,
                        );
                        isLoading.value = false;

                        Get.back();
                      } catch (e) {
                        isLoading.value = false;

                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text("Error"),
                            duration: Duration(milliseconds: 3000),
                          ),
                        );

                        print(e);
                      }
                    } else {
                      print('Hay errores');
                      isLoading.value = false;
                    }
                  },
                  isLoading: isLoading,
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  alignment: Alignment.bottomCenter,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      //backgroundColor: const Color.fromRGBO(255, 87, 87, 1),
                      shape: RoundedRectangleBorder(
                          //to set border radius to button
                          borderRadius: BorderRadius.circular(30)),
                    ),
                    onPressed: () {
                      auth.signOut().then((value) {
                        Navigator.of(context).pushNamed("/init");
                      });
                    },
                    child: const Text('Log out'),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> onPressedLogOut(BuildContext context) async {
    await FirebaseAuth.instance
        .signOut()
        .then((value) => Navigator.of(context).pushNamed("/login"));
  }

  pickLogoPicture(bool isCamera, int profilePicture) async {
    final ImagePicker _picker = ImagePicker();

    if (await Permission.mediaLibrary.request().isGranted &&
        await Permission.camera.request().isGranted) {
      try {
        final result = await _picker.pickImage(
            source: isCamera ? ImageSource.camera : ImageSource.gallery);

        setState(() {});
      } catch (e) {
        print(e);
      }
    } else {}
  }
}
