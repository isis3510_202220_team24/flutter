import 'dart:io';

import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:petsbnb/models/imagen.dart';
import 'package:petsbnb/models/place.dart';
import 'package:petsbnb/services/model_service/place_service.dart';
import 'package:petsbnb/widgets/boton_crear.dart';
import 'package:petsbnb/widgets/custom_text_field.dart';

class AddPlaceView extends StatefulWidget {
  AddPlaceView({Key? key}) : super(key: key);

  @override
  State<AddPlaceView> createState() => _AddPlaceViewState();
}

class _AddPlaceViewState extends State<AddPlaceView> {
  File? file1;
  File? file2;
  File? file3;
  final TextEditingController nameController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();
  final TextEditingController barrioController = TextEditingController();
  final TextEditingController ciudadController = TextEditingController();
  final TextEditingController priceController = TextEditingController();
  final TextEditingController addressController = TextEditingController();

  String errorText = '';
  RxBool isLoading = false.obs;
  var key = GlobalKey<FormState>();
  bool imageError = false;
  @override
  void initState() {
    Get.testMode = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'Crear Lugar',
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 20,
          ),
        ),
      ),
      body: Form(
        key: key,
        child: SafeArea(
          child: SingleChildScrollView(
              child: Column(
            children: [
              const SizedBox(height: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ImageContainer(
                    file: file1,
                    onTap: () {
                      pickLogoPicture(true, 1);
                    },
                  ),
                  ImageContainer(
                    file: file2,
                    onTap: () {
                      pickLogoPicture(true, 2);
                    },
                  ),
                  ImageContainer(
                    file: file3,
                    onTap: () {
                      pickLogoPicture(true, 3);
                    },
                  )
                ],
              ),
              const SizedBox(height: 5),
              Text(
                errorText,
                style: const TextStyle(
                  fontSize: 14,
                  color: Colors.red,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 40),
              CustomTextField(
                textEditingController: nameController,
                titleText: "Nombre",
              ),
              const SizedBox(height: 20),
              CustomTextField(
                textEditingController: descriptionController,
                titleText: "Descripcion",
                maxLines: 3,
              ),
              const SizedBox(height: 20),
              CustomTextField(
                textEditingController: priceController,
                titleText: "Precio",
                keyboardType: TextInputType.number,
              ),
              const SizedBox(height: 20),
              CustomTextField(
                textEditingController: ciudadController,
                titleText: "Ciudad",
              ),
              CustomTextField(
                textEditingController: barrioController,
                titleText: "Barrio",
              ),
              CustomTextField(
                textEditingController: addressController,
                titleText: "Direccion",
              ),
              const SizedBox(height: 30),
              CustomButton(
                buttonText: 'Crear lugar',
                onPressed: () async {
                  isLoading.value = true;
                  if (file1 != null && file2 != null && file3 != null) {
                    setState(() {
                      errorText = '';
                    });
                  } else {
                    setState(() {
                      errorText = 'Por favor ingresa todas las imágenes';
                    });
                    isLoading.value = false;

                    return;
                  }
                  if (key.currentState!.validate()) {
                    try {
                      print(1);

                      List<Location> locations = await locationFromAddress(
                          addressController.text + " " + ciudadController.text);

                      List<Imagen> imagenes = [
                        Imagen(archivo: file1, order: 1),
                        Imagen(archivo: file2, order: 2),
                        Imagen(archivo: file3, order: 3),
                      ];
                      Ubicacion ubicacion = Ubicacion(
                        latitud: locations[0].latitude,
                        longitud: locations[0].longitude,
                      );
                      Place place = Place(
                        titulo: nameController.text,
                        descripcion: descriptionController.text,
                        ciudad: ciudadController.text,
                        barrio: barrioController.text,
                        ubicacion: ubicacion,
                        precio: int.parse(priceController.text),
                      );

                      await placeService.addPlace(
                        place: place.toJson(),
                        imagenes: imagenes,
                      );
                      isLoading.value = false;

                      //Get.back();
                      //await Navigator.of(context).pushNamed("/searchView");
                      await Navigator.of(context).pushNamed("/nav");
                    } catch (e) {
                      isLoading.value = false;
                      if (e.toString() ==
                          'Could not find any result for the supplied address or coordinates.') {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content:
                                Text("Error, por favor verifica tu dirección"),
                            duration: Duration(milliseconds: 3000),
                          ),
                        );
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text("Error"),
                            duration: Duration(milliseconds: 3000),
                          ),
                        );
                      }

                      print(e);
                    }
                  } else {
                    print('Hay errores');
                    isLoading.value = false;
                  }
                },
                isLoading: isLoading,
              ),
              const SizedBox(height: 30),
            ],
          )),
        ),
      ),
    );
  }

  pickLogoPicture(bool isCamera, int profilePicture) async {
    final ImagePicker _picker = ImagePicker();

    if (await Permission.mediaLibrary.request().isGranted &&
        await Permission.camera.request().isGranted) {
      try {
        final result = await _picker.pickImage(
            source: isCamera ? ImageSource.camera : ImageSource.gallery);

        setState(() {
          if (result != null) {
            if (profilePicture == 1) {
              file1 = File(result.path);
            } else if (profilePicture == 2) {
              file2 = File(result.path);
            } else {
              file3 = File(result.path);
            }
          } else {}
        });
      } catch (e) {
        print(e);
      }
    } else {}
  }
}

class ImageContainer extends StatelessWidget {
  ImageContainer({
    super.key,
    required this.file,
    required this.onTap,
  });
  File? file;
  void Function() onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: Get.width * 0.25,
        width: Get.width * 0.25,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            border: Border.all(color: Colors.black)),
        child: file == null
            ? const Center(child: Icon(Icons.add))
            : ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Image.file(file!, fit: BoxFit.cover),
              ),
      ),
    );
  }
}
