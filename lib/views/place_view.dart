import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:petsbnb/models/place.dart';

import '../services/model_service/place_service.dart';
import '../widgets/boton_crear.dart';
import '../widgets/connectivity.dart';

class PlacesListPage extends StatefulWidget {
  @override
  _PlacesListPageState createState() => _PlacesListPageState();
}

class _PlacesListPageState extends State<PlacesListPage> {
  List<Place> places = [];
  RxBool isLoading = false.obs;
  @override
  void initState() {
    getData();

    super.initState();
  }

  getData() async {
    setState(() {
      isLoading.value = true;
    });

    bool result = await connectionStatus.getNormalStatus();

    if (result) {
      try {
        places = await placeService.getPlaces();

        setState(() {
          isLoading.value = false;
        });
      } on Exception catch (e) {
        print(e);
        setState(() {
          isLoading.value = false;
        });
      }
    } else {
      setState(() {
        isLoading.value = false;
      });
      //CustomSnackBars.showErrorSnackBar("No tienes internet, intentalo más tarde");
      print("No hay internet");
      AlertDialog alert = AlertDialog(
        title: Text("¡Sin Conexion!"),
        content: Text("Actualmente no tienes internet, revisa tu conexion"),
        actions: [
          CustomButton(
            buttonText: "Home",
            onPressed: () async {
              await Navigator.of(context).pushNamed("/nav");
              getData();
            },
            isLoading: false.obs,
          ),
          CustomButton(
            buttonText: "Reintentar",
            onPressed: () async {
              await Navigator.of(context).pushNamed("/searchView");
              getData();
            },
            isLoading: false.obs,
          ),
        ],
      );

      // show the dialog
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }

    //isLoading.value = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Obx(() => isLoading.value
            ? const Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                child: Column(
                children: [
                  SizedBox(
                    height: 260.0 * places.length,
                    child: ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        var place = places[index];
                        return Column(
                          children: [
                            CarouselSlider.builder(
                              itemCount: place.imagenes!.length,
                              itemBuilder: (BuildContext context, int itemIndex,
                                      int pageViewIndex) =>
                                  Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.5,
                                      color: Colors.blue,
                                      // margin: EdgeInsets.symmetric(horizontal: 5.0),
                                      // decoration: BoxDecoration(
                                      //   color: Colors.amber
                                      // ),
                                      //child: Image.network(place.imagenes![itemIndex].url!)),
                                      child: CachedNetworkImage(
                                          imageUrl:
                                              place.imagenes![itemIndex].url!)),
                              options: CarouselOptions(),
                            ),
                            SizedBox(
                              height: 40,
                              child: ListTile(
                                title: Text(place.titulo!),
                              ),
                            ),
                          ],
                        );
                      },
                      itemCount: places.length,
                    ),
                  ),
                  Center(
                    child: CustomButton(
                      buttonText: "add place",
                      onPressed: () async {
                        await Navigator.of(context).pushNamed("/addPlace");
                        getData();
                      },
                      isLoading: false.obs,
                    ),
                  )
                ],
              ))),
      ),
    );
  }
}
