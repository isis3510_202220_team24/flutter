
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:petsbnb/models/place.dart';
import '../services/model_service/place_service.dart';
import '../widgets/boton_crear.dart';
import '../widgets/connectivity.dart';

class PlacesListPage2 extends StatefulWidget {
  @override
  _PlacesListPageState2 createState() => _PlacesListPageState2();
}

class _PlacesListPageState2 extends State<PlacesListPage2> {
  List<Place> places = [];
  RxBool isLoading = false.obs;
  @override
  void initState() {
    getData();

    super.initState();
  }

  getData() async {
    setState(() {
      isLoading.value = true;
    });

    bool result = await connectionStatus.getNormalStatus();

    if (result) {
      final box = GetStorage();

      if (box.read('places') == null) {
        try {
          places = await placeService.getPlaces();

          setState(() {
            isLoading.value = false;
          });
        } on Exception catch (e) {
          print(e);
          setState(() {
            isLoading.value = false;
          });
        }
      } else {
        final List<dynamic> aux = box.read('places');
        print('auuuuux: ${aux.length}');
        aux.forEach((element) {
          places.add(Place(
            id: element.id,
            titulo: element.titulo,
            descripcion: element.descripcion,
            ciudad: element.ciudad,
            barrio: element.barrio,
            ubicacion: element.ubicacion,
            precio: element.precio,
          ));
        });
      }
    } else {
      setState(() {
        isLoading.value = false;
      });
      //CustomSnackBars.showErrorSnackBar("No tienes internet, intentalo más tarde");
      print("No hay internet");
      AlertDialog alert = AlertDialog(
        title: const Text("¡Sin Conexion!"),
        content:
            const Text("Actualmente no tienes internet, revisa tu conexion"),
        actions: [
          CustomButton(
            buttonText: "Home",
            onPressed: () async {
              await Navigator.of(context).pushNamed("/nav");
              getData();
            },
            isLoading: false.obs,
          ),
          CustomButton(
            buttonText: "Reintentar",
            onPressed: () async {
              await Navigator.of(context).pushNamed("/searchView");
              getData();
            },
            isLoading: false.obs,
          ),
        ],
      );

      // show the dialog
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }

    //isLoading.value = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Obx(() => isLoading.value
            ? const Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                child: Column(
                children: [
                  SizedBox(
                    height: 260.0 * places.length,
                    child: ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        var place = places[index];
                        return Stack(
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.fromLTRB(
                                  40.0, 5.0, 20.0, 5.0),
                              height: 170.0,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(
                                    100.0, 20.0, 20.0, 20.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          width: 100.0,
                                          child: Text(
                                            place.titulo!,
                                            style: const TextStyle(
                                              fontSize: 18.0,
                                              fontWeight: FontWeight.w600,
                                            ),
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                          ),
                                        ),
                                        Column(
                                          children: <Widget>[
                                            Text(
                                              '\$${place.precio}',
                                              style: const TextStyle(
                                                fontSize: 18.0,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                            const Text(
                                              'Por dia',
                                              style: TextStyle(
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    Text(
                                      place.descripcion!,
                                      style: const TextStyle(
                                        color: Colors.grey,
                                      ),
                                    ),
                                    //_buildRatingStars(activity.rating),
                                    const SizedBox(height: 10.0),
                                    Row(
                                      children: <Widget>[
                                        Container(
                                          padding: const EdgeInsets.all(5.0),
                                          width: 70.0,
                                          decoration: BoxDecoration(
                                            color:
                                                Theme.of(context).accentColor,
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                          ),
                                          alignment: Alignment.center,
                                          child: Text(
                                            place.barrio!,
                                          ),
                                        ),
                                        const SizedBox(width: 10.0),
                                        Container(
                                          padding: const EdgeInsets.all(5.0),
                                          width: 70.0,
                                          decoration: BoxDecoration(
                                            color:
                                                Theme.of(context).accentColor,
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                          ),
                                          alignment: Alignment.center,
                                          child: Text(
                                            place.ciudad!,
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Positioned(
                              left: 20.0,
                              top: 15.0,
                              bottom: 15.0,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(20.0),
                                child: CachedNetworkImage(
                                    imageUrl: place.imagenes![0].url!,
                                    width: 95,
                                    fit: BoxFit.fill),
                                //
                                // ,
                                // child: CarouselSlider.builder(
                                //   itemCount: place.imagenes!.length,
                                //   itemBuilder: (BuildContext context,
                                //           int itemIndex, int pageViewIndex) =>
                                //       Container(
                                //           width: 85,
                                //           color: Colors.blue,
                                //           // margin: EdgeInsets.symmetric(horizontal: 5.0),
                                //           // decoration: BoxDecoration(
                                //           //   color: Colors.amber
                                //           // ),
                                //           //child: Image.network(place.imagenes![itemIndex].url!)),
                                //           child: CachedNetworkImage(
                                //               imageUrl: place
                                //                   .imagenes![itemIndex].url!,
                                //               fit: BoxFit.fill)),
                                //   options: CarouselOptions(),
                                // ),
                              ),
                            ),
                          ],
                        );
                      },
                      itemCount: places.length,
                    ),
                  ),
                  Center(
                    child: CustomButton(
                      buttonText: "add place",
                      onPressed: () async {
                        await Navigator.of(context).pushNamed("/addPlace");
                        getData();
                      },
                      isLoading: false.obs,
                    ),
                  )
                ],
              ))),
      ),
    );
  }
}
